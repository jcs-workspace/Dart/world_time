# world_time

Following the tutorial [Flutter Tutorial for Beginners #22 - Starting the World Time App](https://www.youtube.com/watch?v=WghpP9W2vXo&list=PL4cUxeGkcC9jLYyp2Aoh6hcWuxFDX6PBJ&index=23).

## API links

- https://jsonplaceholder.typicode.com/
- https://worldtimeapi.org/
