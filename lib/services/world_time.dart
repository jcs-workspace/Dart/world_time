import 'package:http/http.dart' as http;
import 'package:intl/intl.dart' as intl;
import 'dart:convert';

class WorldTime {
  String location;
  String time = '';
  String flag;
  String url;
  bool isDaytime = false;

  WorldTime({required this.location, required this.flag, required this.url});

  Future<void> getTime() async {
    try {
      var uri = Uri.https('worldtimeapi.org', 'api/timezone/$url');
      http.Response response = await http.get(uri);
      Map data = jsonDecode(response.body);

      String datetime = data['datetime'];
      String offset = data['utc_offset'].substring(1, 3);

      var now = DateTime.parse(datetime);
      now = now.add(Duration(hours: int.parse(offset)));

      // set the time property
      isDaytime = now.hour > 6 && now.hour < 15 ? true : false;
      time = intl.DateFormat.jm().format(now);
    } catch (e) {
      print('caught error: $e');
      time = 'could not get time data';
    }
  }
}
